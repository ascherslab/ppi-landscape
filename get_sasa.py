from Bio.PDB import NeighborSearch, PDBIO, PDBParser, PDBList, Select, Selection, DSSP, ResidueDepth
from Bio.PDB.Polypeptide import three_to_one, is_aa
from collections import Counter
from subprocess import Popen, PIPE

import math
import ntpath
import os
import pandas as pd
import pickle
import sys
import warnings

warnings.filterwarnings("ignore")

VALID_PAIRS = ""
NACCESS_BIN = ""

class ChainSelect(Select):
    def __init__(self, chain):
        self.chain = chain

    def accept_chain(self, chain):
        if chain.get_id() == self.chain:
            return 1
        else:
            return 0

def get_interface(pdb_file):
    pdb_identifier = ntpath.basename(pdb_file).replace(".pdb","")
    pdb_code = pdb_identifier.split("_")[1]
    chain1 = pdb_identifier.split("_")[-2]
    chain2 = pdb_identifier.split("_")[-1]

    parser = PDBParser()
    structure = parser.get_structure(pdb_identifier, pdb_file)
    model = structure[0]

    interface1 = model[chain1]
    interface2 = model[chain2]

    residues_interface_chain2 = set()
    atoms_list = Selection.unfold_entities(interface2,'A')
    ns = NeighborSearch(atoms_list)
    neighbors = None
    for atom in interface1.get_atoms():
        center = atom.get_coord()
        neighbors = ns.search(center,5.0,level='R')
        if neighbors:
            residues_interface_chain2.update(neighbors)

    residues_interface_chain1 = set()
    atoms_list = Selection.unfold_entities(interface1,'A')
    ns = NeighborSearch(atoms_list)
    neighbors = None
    for atom in interface2.get_atoms():
        center = atom.get_coord()
        neighbors = ns.search(center,5.0,level='R')
        if neighbors:
            residues_interface_chain1.update(neighbors)

    residues_interface_chain1 = list(residues_interface_chain1)
    residues_interface_chain2 = list(residues_interface_chain2)
    residues_interface_chain1.sort()
    residues_interface_chain2.sort()
    seq1 = ["{}/{}/{}".format(item.parent.id, item.id[1], item.resname) \
            for item in residues_interface_chain1]
    seq2 = ["{}/{}/{}".format(item.parent.id, item.id[1], item.resname) \
            for item in residues_interface_chain2]

    return seq1,seq2

def parse_sasa_file(sasa_file):
    cmd = "cut -c55-69 {} | awk '{{print $1}}' | paste -sd+ | bc".format(sasa_file)
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
    out,err = p.communicate()
    overall_sasa = float(out.decode('utf-8'))

    pdb_identifier = ntpath.basename(sasa_file).replace(".sasa","")

    parser = PDBParser()
    structure = parser.get_structure(pdb_identifier, sasa_file)
    model = structure[0]
    residue_wise = {}
    for residue in model.get_residues():
        key = '{}_{}'.format(residue.parent.id,residue.id[1])
        residue_wise[key] = 0.0
        for atom in residue.get_atoms():
            residue_wise[key] += atom.occupancy

    return overall_sasa,residue_wise

def parse_rsa_file(rsa_file):
    cmd = "grep 'Absolute sums over all chains' {} -A 1 | tail -n1 | awk '{{print $3}}'".format(rsa_file)
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
    out,err = p.communicate()
    overall_rsa = float(out.decode('utf-8'))

    cmd = "grep -e '^RES' {}".format(rsa_file)
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
    out,err = p.communicate()
    out = out.decode('utf-8').strip()
    residue_wise = {}
    for line in out.split('\n'):
        chain = line[8]
        parsed_line = line[9:].strip().split()
        residue_position = parsed_line[0]
        residue_wise['{}_{}'.format(chain,residue_position)] = float(parsed_line[2])

    return overall_rsa,residue_wise

def run_naccess(pdb_file):
    pdb_identifier = ntpath.basename(pdb_file).replace(".pdb","")
    cmd = "{} {}".format(NACCESS_BIN, pdb_file)
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
    out,err = p.communicate()

    log_file = os.path.join(os.getcwd(),"{}.log".format(pdb_identifier))
    asa_file = os.path.join(os.getcwd(),"{}.asa".format(pdb_identifier))
    rsa_file = os.path.join(os.getcwd(),"{}.rsa".format(pdb_identifier))

    return log_file,asa_file,rsa_file

def cleanup(files):
    cmd = "rm {}".format(" ".join(files))
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
    out,err = p.communicate()
    return out,err

def main():
    interfaces_file = sys.argv[1]
    interface_clusters = pickle.load(open(interfaces_file,"rb"))
    
    parser = PDBParser()
    io_selection = PDBIO()
    features_dict = {}
    for cluster_id,pdb_file in interface_clusters.items():
        features_dict[cluster_id] = {}

        pdb_identifier = ntpath.basename(pdb_file).replace("interface_","").replace(".pdb","")
        pdb_code = pdb_identifier.split("_")[0]
        chain1 = pdb_identifier.split("_")[1]
        chain2 = pdb_identifier.split("_")[2]
        parsed_pdb_file = os.path.join(VALID_PAIRS,"interface_{}_{}_{}.pdb".format(pdb_code,chain1,chain2))

        pdb_file_chain1 = os.path.join(os.getcwd(),"interface_{}_{}_{}-{}.pdb".format(pdb_code,chain1,chain2,chain1))
        pdb_file_chain2 = os.path.join(os.getcwd(),"interface_{}_{}_{}-{}.pdb".format(pdb_code,chain1,chain2,chain2))

        log_file,asa_file,rsa_file = run_naccess(parsed_pdb_file)
        try:
            total_sasa,sasa_residue_wise = parse_sasa_file(asa_file)
            total_rsa,rsa_residue_wise = parse_rsa_file(rsa_file)

            log_file_chain1,asa_file_chain1,rsa_file_chain1 = run_naccess(pdb_file_chain1)
            total_sasa_chain1,sasa_residue_wise_chain1 = parse_sasa_file(asa_file_chain1)
            total_rsa_chain1,rsa_residue_wise_chain1 = parse_rsa_file(rsa_file_chain1)

            log_file_chain2,asa_file_chain2,rsa_file_chain2 = run_naccess(pdb_file_chain2)
            total_sasa_chain2,sasa_residue_wise_chain2 = parse_sasa_file(asa_file_chain2)
            total_rsa_chain2,rsa_residue_wise_chain2 = parse_rsa_file(rsa_file_chain2)

            delta_sasa = total_sasa_chain1 + total_sasa_chain2 - total_sasa

            features_dict[cluster_id]['delta_sasa'] = delta_sasa
            features_dict[cluster_id]['sasa_complex'] = total_sasa
            features_dict[cluster_id]['sasa_chain1'] = total_sasa_chain1
            features_dict[cluster_id]['sasa_chain2'] = total_sasa_chain2
            features_dict[cluster_id]['sasa_complex_residuewise'] = sasa_residue_wise
            features_dict[cluster_id]['rsa_complex_residuewise'] = rsa_residue_wise
            features_dict[cluster_id]['rsa_complex_residuewise_chain1'] = rsa_residue_wise_chain1
            features_dict[cluster_id]['rsa_complex_residuewise_chain2'] = rsa_residue_wise_chain2
            features_dict[cluster_id]['sasa_complex_residuewise_chain1'] = sasa_residue_wise_chain1
            features_dict[cluster_id]['sasa_complex_residuewise_chain2'] = sasa_residue_wise_chain2

        except Exception as e:
            print(e)
            pass
            print(parsed_pdb_file)

    file_identifier = ntpath.basename(interfaces_file).replace(".pkl","")
    pickle_file = "interfaces_clusters/{}_sasa_rsa.pkl".format(file_identifier)
    pickle.dump(features_dict, open(pickle_file,"wb"))
    print(pickle_file)

    return True


if __name__ == "__main__":
    main()
