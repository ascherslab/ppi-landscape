import os
import pickle
import sys

def main():
    output_cdhit = sys.argv[1]
    output_lines = [line.strip() for line in open(output_cdhit,"r").readlines()]

    cluster_dict = {}

    cluster_key = int()
    for line in output_lines:
        if line.startswith(">Cluster"):
            cluster_key = int(line.split()[-1])
        else:
            parsed_line = line.split()[2][1:-3]
            cluster_dict[parsed_line] = cluster_key
    
    pickle.dump(cluster_dict,open("cdhit_clusters.pkl","wb"))
    return True


if __name__ == "__main__":
    main()
