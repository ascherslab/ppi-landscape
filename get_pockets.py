from Bio.PDB import NeighborSearch, PDBIO, PDBParser, PDBList, \
        Select, Selection, DSSP, ResidueDepth
from Bio.PDB.ResidueDepth import get_surface, residue_depth, ca_depth
from Bio.PDB.Polypeptide import three_to_one, is_aa
from collections import Counter
from subprocess import Popen, PIPE

import math
import ntpath
import numpy as np
import os
import pandas as pd
import pickle
import sys
import warnings

warnings.filterwarnings("ignore")

VALID_PAIRS = ""

def get_interface(pdb_file):
    pdb_identifier = ntpath.basename(pdb_file).replace(".pdb","")
    pdb_code = pdb_identifier.split("_")[1]
    chain1 = pdb_identifier.split("_")[-2]
    chain2 = pdb_identifier.split("_")[-1]

    parser = PDBParser()
    structure = parser.get_structure(pdb_identifier, pdb_file)
    model = structure[0]

    interface1 = model[chain1]
    interface2 = model[chain2]

    residues_interface_chain2 = set()
    atoms_list = Selection.unfold_entities(interface2,'A')
    ns = NeighborSearch(atoms_list)
    neighbors = None
    for atom in interface1.get_atoms():
        center = atom.get_coord()
        neighbors = ns.search(center,5.0,level='R')
        if neighbors:
            residues_interface_chain2.update(neighbors)

    residues_interface_chain1 = set()
    atoms_list = Selection.unfold_entities(interface1,'A')
    ns = NeighborSearch(atoms_list)
    neighbors = None
    for atom in interface2.get_atoms():
        center = atom.get_coord()
        neighbors = ns.search(center,5.0,level='R')
        if neighbors:
            residues_interface_chain1.update(neighbors)

    residues_interface_chain1 = list(residues_interface_chain1)
    residues_interface_chain2 = list(residues_interface_chain2)
    residues_interface_chain1.sort()
    residues_interface_chain2.sort()
    seq1 = ["{}/{}/{}".format(item.parent.id, item.id[1], item.resname) \
            for item in residues_interface_chain1]
    seq2 = ["{}/{}/{}".format(item.parent.id, item.id[1], item.resname) \
            for item in residues_interface_chain2]

    return {chain1: seq1, chain2: seq2}

def get_ghecom(pdb_file, chain, residues):
    ghecom_atoms = pdb_file.replace(".pdb","-{}_ghecom.atom".format(chain))
    ghecom_residues = pdb_file.replace(".pdb","-{}_ghecom.res".format(chain))
    cmd = "ghecom {} -M M -ch {} -ah A -opdb {} -ores {}".format(pdb_file, chain, \
                                                                 ghecom_atoms, ghecom_residues)
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
    out,err = p.communicate()

    output = {}
    averages = []
    deepests = []
    for residue in residues:
        _,position,resname = residue.split("/")
        output[residue] = {}

        cmd = "awk {{'if($1 == \"{}\"){{print}}'}} {}".format(position, ghecom_residues)
        p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
        out,err = p.communicate()
        parsed_out = out.decode("utf-8").strip().split()
        try:
            output[residue]['avg_rinacc'] = float(parsed_out[4])
            averages.append(float(parsed_out[4]))
        except Exception as e:
            output[residue]['avg_rinacc'] = 'error'

        cmd = "grep -E \"^ATOM\" {} | grep \"{}\" | grep -E \"^.{{21}}{}\" | grep -E \"^.{{22}}{:>4}\" | cut -c73-78".format(ghecom_atoms, \
                                                                                                            resname, chain, \
                                                                                                            position)
        p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
        out,err = p.communicate()
        try:
            parsed_out = [float(item) for item in out.decode("utf-8").strip().split("\n")]
            output[residue]['deepest_rinacc'] = float(np.min(parsed_out))
            deepests.append(float(np.min(parsed_out)))
        except Exception as e:
            output[residue]['deepest_rinacc'] = 'error'

    output['average_overall'] = np.mean(averages)
    output['deepest_overall'] = np.min(deepests)
    return output

def main():
    interfaces_file = sys.argv[1]
    interface_clusters = pickle.load(open(interfaces_file,"rb"))

    parser = PDBParser()
    io_selection = PDBIO()
    features_dict = {}
    for cluster_id,pdb_file in interface_clusters.items():
        features_dict[cluster_id] = {}

        pdb_identifier = ntpath.basename(pdb_file).replace("interface_","").replace(".pdb","")
        pdb_code = pdb_identifier.split("_")[0]
        chain1 = pdb_identifier.split("_")[1]
        chain2 = pdb_identifier.split("_")[2]
        parsed_pdb_file = os.path.join(VALID_PAIRS,"interface_{}_{}_{}.pdb".format(pdb_code,chain1,chain2))

        residues_side1,residues_side2 = get_interface(parsed_pdb_file)
        interface_residues = get_interface(parsed_pdb_file)
        interface_residues = residues_side1 + residues_side2

        output_chain1 = get_ghecom(parsed_pdb_file, chain1, interface_residues[chain1])
        output_chain2 = get_ghecom(parsed_pdb_file, chain2, interface_residues[chain2])

        if len(interface_residues[chain1]) < len(interface_residues[chain1]):
            features_dict[cluster_id]['smaller_side'] = output_chain1
            features_dict[cluster_id]['larger_side'] = output_chain2
        else:
            features_dict[cluster_id]['smaller_side'] = output_chain2
            features_dict[cluster_id]['larger_side'] = output_chain1

    file_identifier = ntpath.basename(interfaces_file).replace(".pkl","")
    pickle_file = "interfaces_clusters/{}_pockets.pkl".format(file_identifier)
    pickle.dump(features_dict, open(pickle_file,"wb"))
    print(pickle_file)

    return True


if __name__ == "__main__":
    main()
