from Bio.PDB import NeighborSearch, PDBIO, PDBParser, PDBList, \
        Select, Selection, DSSP, ResidueDepth
from Bio.PDB.ResidueDepth import get_surface, residue_depth, ca_depth
from Bio.PDB.Polypeptide import three_to_one, is_aa
from collections import Counter
from subprocess import Popen, PIPE

import math
import ntpath
import numpy as np
import os
import pandas as pd
import pickle
import sys
import warnings

warnings.filterwarnings("ignore")

VALID_PAIRS = ""
NACCESS_PATH = ""

def main():
    interfaces_file = sys.argv[1]
    interface_clusters = pickle.load(open(interfaces_file,"rb"))

    parser = PDBParser()
    io_selection = PDBIO()
    features_dict = {}
    for cluster_id,pdb_file in interface_clusters.items():
        features_dict[cluster_id] = {}

        pdb_identifier = ntpath.basename(pdb_file).replace("interface_","").replace(".pdb","")
        pdb_code = pdb_identifier.split("_")[0]
        chain1 = pdb_identifier.split("_")[1]
        chain2 = pdb_identifier.split("_")[2]
        parsed_pdb_file = os.path.join(VALID_PAIRS,"interface_{}_{}_{}.pdb".format(pdb_code,chain1,chain2))

        cmd = "cat /dev/null > pdblist"
        p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
        out,err = p.communicate()

        output_file = os.path.join(os.getcwd(),"NIP_NSc_score.txt")
        cmd = "cat /dev/null > {}".format(output_file)
        p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
        out,err = p.communicate()

        pdblist_file = os.path.join(os.getcwd(),'pdblist')
        cmd = "echo \"interface_{}_{}_{}.pdb\" > {}".format(pdb_code, chain1, chain2, pdblist_file)
        p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
        out,err = p.communicate()

        cmd = "nip_nsc {} {}".format(VALID_PAIRS, NACCESS_PATH) 
        p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
        out,err = p.communicate()

        cmd = "grep interface_{}_{}_{}.pdb {}".format(pdb_code,chain1,chain2,output_file)
        p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
        out,err = p.communicate()

        try:
            parsed_output = out.decode("utf-8").strip().split()
            nip = parsed_output[2]
            nsc = parsed_output[3]
        except Exception as e:
            nip = 'error'
            nsc = 'error'

        features_dict[cluster_id]['nip'] = nip
        features_dict[cluster_id]['nsc'] = nsc

    file_identifier = ntpath.basename(interfaces_file).replace(".pkl","")
    pickle_file = "interfaces_clusters/{}_nip.pkl".format(file_identifier)
    pickle.dump(features_dict, open(pickle_file,"wb"))

    return True

if __name__ == "__main__":
    main()
