from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC
from Bio.Seq import Seq
from Bio.PDB.Polypeptide import three_to_one, is_aa
from Bio.PDB import NeighborSearch, PDBIO, PDBParser, PDBList, Select, Selection, DSSP, ResidueDepth
from django.core.management.base import BaseCommand
from subprocess import Popen, PIPE

import itertools
import ntpath
import numpy as np
import os
import sys
import warnings

warnings.filterwarnings("ignore")

ALL_PDBS_FOLDER = ''
BIOUNITS_FOLDER = ''
PPI_INTERFACES = ''
VALID_INTERFACES = ''
PDB_GAP_BIN = ''

def get_gaps(pdb_file):
    cmd = "{} {}".format(PDB_GAP_BIN, pdb_file)
    p = Popen(cmd,shell=True,stdin=PIPE,stdout=PIPE)
    out,err = p.communicate()
    out = out.decode("utf-8")

    gap_dict = {}
    if not out.startswith("Found 0 gap"):
        lines = out.strip().split("\n")[:-1]
        for line in lines:
            chain = line[0]
            res_from = int(line.split()[0].split(":")[-1][3:])
            res_to = int(line.split()[-1].split(":")[-1][3:])
            if chain in gap_dict:
                gap_dict[chain].append(range(res_from,res_to))
            else:
                gap_dict[chain] = [range(res_from,res_to)]

    return gap_dict

def get_pdb_sequence(pdb_file, chain=""):
    p = PDBParser(PERMISSIVE=1)
    structure = p.get_structure(pdb_file, pdb_file)
    seq = list()
    model = structure[0]

    for residue in model[chain].get_residues():
        if is_aa(residue.get_resname(), standard=True):
            seq.append(three_to_one(residue.get_resname()))

    sequence = str("".join(seq))

    return sequence

def get_biounits(pdb_file):
    biounits_list = list()

    cmd = "grep \"REMARK 350 BIOMOLECULE\" -A 1 {}".format(pdb_file)
    p1 = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
    out,err = p1.communicate()
    output = out.decode('utf-8').split("\n")

    pdb_code = ntpath.basename(pdb_file).replace(".ent","")[3:]
    subfolder = pdb_code[1:-1]
    for i in range(0, len(output)):
        line = output[i].strip()
        if len(line) > 0 and "REMARK 350 BIOMOLECULE" in line:
            if line.split()[-1].isdigit():
                original = os.path.join(BIOUNITS_FOLDER, subfolder, "{}.pdb{}.gz".format(str(pdb_code).lower(), line.split()[-1]))
                biological_unit_pdb = os.path.join(ALL_PDBS_FOLDER, "{}.pdb{}".format(str(pdb_code).lower(), line.split()[-1]))

                cmd = "gunzip -c {} > {}".format(original, biological_unit_pdb)
                p1 = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
                out,err = p1.communicate()

                author_assigned = False
                if "AUTHOR" in output[i+1]:
                    author_assigned = True
                biounits_list.append({
                    'biounit': biological_unit_pdb,
                    'author_assigned': author_assigned,
                })

    return biounits_list

def chain_is_na(chain):
    na_list = ['A','C','G','U','T','DA','DC','DG','DU','DT','OMA','OMC','OMG','OMU','OMT']

    residue_list = [item.resname.strip() for item in Selection.unfold_entities(chain, 'R')]
    if any(i in na_list for i in residue_list):
        return True

    return False

class PairwiseChainSelect(Select):
    def __init__(self, chain1, chain2):
        self.chain1 = chain1
        self.chain2 = chain2

    def accept_chain(self, chain):
        if (chain.get_id() == self.chain1) or (chain.get_id() == self.chain2):
            return 1
        else:
            return 0

    def accept_residue(self, residue):
        if residue.id[0] == " " and is_aa(residue.get_resname(), standard=True):
            return 1
        else:
            return 0

    def accept_model(self, model):
        if model.id == 0:
            return 1
        else:
            return 0

    def accept_atom(self, atom):
        if atom.element == 'H':
            return 0
        elif (not atom.is_disordered()) or atom.get_altloc() == 'A':
            atom.set_altloc(' ')
            return 1
        else:
            return 0

def main():
    original_pdb = sys.argv[1]

    pdb_file = original_pdb.replace(".gz","")
    pdb_file_identifier = ntpath.basename(pdb_file)
    pdb_code = pdb_file_identifier.split('.')[0][3:]

    cmd = "gunzip -c {} > {}".format(original_pdb, pdb_file)
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
    out,err = p.communicate()

    output_biounits = get_biounits(pdb_file)
    if len(output_biounits) != 0:
        for item in output_biounits:
            if item['author_assigned']:
                pdb_file = item['biounit']

    parser = PDBParser()
    structure = parser.get_structure(pdb_code, pdb_file)

    try:
        model = structure[0]
    except KeyError as e:
        print("MODEL 0 PDB - {}".format(pdb_file))
        sys.exit(1)

    model = structure[0]
    chains = list(model.get_chains())

    if len(chains) >= 2:

        io_w_no_h = PDBIO()
        io_w_no_h.set_structure(structure)
        pairs = list(itertools.combinations(chains,2))

        for pair in pairs:
            chain1 = pair[0].id
            chain2 = pair[1].id
            has_nucleic_acids = False
            inter_chain_distance = 9999.99
            interface_label = "{}_{}_{}".format(pdb_code,chain1,chain2)

            if chain_is_na(pair[0]) or chain_is_na(pair[1]):
                has_nucleic_acids = True
            else:
                pairwise_file_identifier = "interface_{}.pdb".format(interface_label)
                pairwise_file = os.path.join(PPI_INTERFACES, pairwise_file_identifier)
                io_w_no_h.save(pairwise_file, PairwiseChainSelect(chain1,chain2))

                parser = PDBParser()
                interface_structure = parser.get_structure(pairwise_file_identifier, pairwise_file)

                try:
                    interface_model = interface_structure[0]
                except KeyError as e:
                    print("POSSIBLE HETATM INTERFACE - {}".format(pairwise_file))
                    continue

                interface_model = interface_structure[0]
                chains_interface = list(interface_model.get_chains())

                if len(chains_interface) == 2:
                    if len(list(interface_model[chain1].get_residues())) > 41 or len(list(interface_model[chain2].get_residues())) > 41:

                        interface1 = interface_model[chain1]
                        interface2 = interface_model[chain2]

                        residues_interface_chain2 = set()
                        atoms_list = Selection.unfold_entities(interface2,'A')
                        ns = NeighborSearch(atoms_list)
                        neighbors = None
                        for atom in interface1.get_atoms():
                            center = atom.get_coord()
                            neighbors = ns.search(center,5.0,level='R')
                            if neighbors:
                                residues_interface_chain2.update(neighbors)

                        residues_interface_chain1 = set()
                        atoms_list = Selection.unfold_entities(interface1,'A')
                        ns = NeighborSearch(atoms_list)
                        neighbors = None
                        for atom in interface2.get_atoms():
                            center = atom.get_coord()
                            neighbors = ns.search(center,5.0,level='R')
                            if neighbors:
                                residues_interface_chain1.update(neighbors)

                        residues_interface_chain1 = list(residues_interface_chain1)
                        residues_interface_chain2 = list(residues_interface_chain2)

                        per_residue_distance = dict()
                        overall_min = 9999.99
                        interface1_residues_to_insert = list()

                        for residue_from in residues_interface_chain1:
                            distances_residue = list()
                            for atom_from in residue_from.get_atoms():
                                for residue_to in residues_interface_chain2:
                                    for atom_to in residue_to.get_atoms():
                                        distance = atom_from-atom_to
                                        if distance < overall_min:
                                            overall_min = distance
                                        distances_residue.append(distance)
                            interface1_residues_to_insert.append(residue_from)

                        interface2_residues_to_insert = list()
                        for residue_from in residues_interface_chain2:
                            distances_residue = list()
                            for atom_from in residue_from.get_atoms():
                                for residue_to in residues_interface_chain1:
                                    for atom_to in residue_to.get_atoms():
                                        distance = atom_from-atom_to
                                        if distance < overall_min:
                                            overall_min = distance
                                        distances_residue.append(distance)
                            interface2_residues_to_insert.append(residue_from)

                        gaps_dict = get_gaps(pairwise_file)
                        gapped_interface = False
                        if chain1 in gaps_dict:
                            for item in residues_interface_chain1:
                                for interval in gaps_dict[item.parent.id]:
                                    if item.id[1] in interval:
                                        gapped_interface = True

                        if chain2 in gaps_dict:
                            for item in residues_interface_chain2:
                                for interval in gaps_dict[item.parent.id]:
                                    if item.id[1] in interval:
                                        gapped_interface = True

                        if not gapped_interface and overall_min <= 5 and (len(interface1_residues_to_insert)*len(interface2_residues_to_insert) >= 25):
                            valid_pairwise_file = os.path.join(VALID_INTERFACES, pairwise_file_identifier)

                            cmd = "mv {} {}".format(pairwise_file,valid_pairwise_file)
                            p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
                            out,err = p.communicate()

                            seq1 = SeqRecord(Seq(get_pdb_sequence(valid_pairwise_file, chain1), IUPAC.protein), \
                                             id="{}-{}".format(pairwise_file_identifier,chain1),description="")
                            seq2 = SeqRecord(Seq(get_pdb_sequence(valid_pairwise_file, chain2), IUPAC.protein), \
                                             id="{}-{}".format(pairwise_file_identifier,chain2),description="")
                            fasta_file = valid_pairwise_file.replace(".pdb",".fasta")
                            SeqIO.write([seq1,seq2], fasta_file,"fasta")

                            residues_file = valid_pairwise_file.replace(".pdb",".res")
                            writer = open(residues_file,"w")
                            for res in interface1_residues_to_insert:
                                writer.write("/{}/{}/\n".format(str(res.id[1]),res.parent.id))
                            for res in interface2_residues_to_insert:
                                writer.write("/{}/{}/\n".format(str(res.id[1]),res.parent.id))
                            writer.close()

    return True

if __name__ == "__main__":
    main()
