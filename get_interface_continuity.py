from Bio.PDB import NeighborSearch, PDBIO, PDBParser, PDBList, Select, Selection, DSSP, ResidueDepth
from Bio.PDB.Polypeptide import three_to_one, is_aa
from collections import Counter
from subprocess import Popen, PIPE

import math
import ntpath
import os
import pandas as pd
import pickle
import sys
import warnings

warnings.filterwarnings("ignore")

VALID_PAIRS = ""

def get_interface(pdb_file):
    pdb_identifier = ntpath.basename(pdb_file).replace(".pdb","")
    pdb_code = pdb_identifier.split("_")[1]
    chain1 = pdb_identifier.split("_")[-2]
    chain2 = pdb_identifier.split("_")[-1]

    parser = PDBParser()
    structure = parser.get_structure(pdb_identifier, pdb_file)
    model = structure[0]

    interface1 = model[chain1]
    interface2 = model[chain2]

    residues_interface_chain2 = set()
    atoms_list = Selection.unfold_entities(interface2,'A')
    ns = NeighborSearch(atoms_list)
    neighbors = None
    for atom in interface1.get_atoms():
        center = atom.get_coord()
        neighbors = ns.search(center,5.0,level='R')
        if neighbors:
            residues_interface_chain2.update(neighbors)

    residues_interface_chain1 = set()
    atoms_list = Selection.unfold_entities(interface1,'A')
    ns = NeighborSearch(atoms_list)
    neighbors = None
    for atom in interface2.get_atoms():
        center = atom.get_coord()
        neighbors = ns.search(center,5.0,level='R')
        if neighbors:
            residues_interface_chain1.update(neighbors)

    residues_interface_chain1 = list(residues_interface_chain1)
    residues_interface_chain2 = list(residues_interface_chain2)
    residues_interface_chain1.sort()
    residues_interface_chain2.sort()
    seq1 = [item.id[1] for item in residues_interface_chain1]
    seq2 = [item.id[1] for item in residues_interface_chain2]

    return {chain1:seq1, chain2:seq2}

def main():
    interfaces_file = sys.argv[1]
    interface_clusters = pickle.load(open(interfaces_file,"rb"))

    segments_dict = {1:[], 2:[], 3:[], 4:[], 5:[], 6:[], 7:[], 8:[], 9:[], 10:[], 11:[], 12:[],}
    
    for cluster_id,pdb_file in interface_clusters.items():
        pdb_identifier = ntpath.basename(pdb_file).replace("interface_","").replace(".pdb","")
        pdb_code = pdb_identifier.split("_")[0]
        chain1 = pdb_identifier.split("_")[1]
        chain2 = pdb_identifier.split("_")[2]
        parsed_pdb_file = os.path.join(VALID_PAIRS,"interface_{}_{}_{}.pdb".format(pdb_code,chain1,chain2))

        interface_residues = get_interface(parsed_pdb_file)

        positions = interface_residues[chain1]
        if len(interface_residues[chain2]) < len(positions):
            positions = interface_residues[chain2]

        segments = list()
        current_segment = [positions[0]]
        for i in range(1,len(positions)):
            if len(current_segment) == 0:
                current_segment.append(positions[i])
            else:
                if abs(positions[i]-current_segment[-1]) > 4:
                    segments.append(current_segment)
                    current_segment = [positions[i]]
                else:
                    current_segment.append(positions[i])
        segments.append(current_segment)
        if (len(segments) != 1):
            if len(segments) in segments_dict:
                segments_dict[len(segments)].append(cluster_id)
            else:
                segments_dict[len(segments)] = [cluster_id]
        else:
            segments_dict[1].append(cluster_id)
        
        segments_txt = '|'.join([','.join([str(item) for item in segment]) for segment in segments])
    file_identifier = ntpath.basename(interfaces_file).replace(".pkl","")
    pickle_file = "interfaces_clusters/{}_segmentation.pkl".format(file_identifier)
    pickle.dump(segments_dict, open(pickle_file,"wb"))
    print(pickle_file)

    return True


if __name__ == "__main__":
    main()
