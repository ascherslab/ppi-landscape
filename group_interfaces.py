from Bio.PDB import NeighborSearch, PDBIO, PDBParser, PDBList, Select, Selection, DSSP, ResidueDepth
from Bio.PDB.Polypeptide import three_to_one, is_aa
from difflib import SequenceMatcher
from subprocess import Popen, PIPE

import math
import ntpath
import os
import pandas as pd
import pickle
import sys
import warnings

warnings.filterwarnings("ignore")

PDB_FOLDER = ''
PDB_GAP_BIN = ''

def get_gaps(pdb_file):
    cmd = "{} {}".format(PDB_GAP_BIN, pdb_file)
    p = Popen(cmd,shell=True,stdin=PIPE,stdout=PIPE)
    out,err = p.communicate()
    out = out.decode("utf-8")

    gap_dict = {}
    if not out.startswith("Found 0 gap"):
        lines = out.strip().split("\n")[:-1]
        for line in lines:
            chain = line[0]
            res_from = int(line.split()[0].split(":")[-1][3:])
            res_to = int(line.split()[-1].split(":")[-1][3:])
            if chain in gap_dict:
                gap_dict[chain].append(range(res_from,res_to))
            else:
                gap_dict[chain] = [range(res_from,res_to)]

    return gap_dict

def get_r_value(pdb_code, label):
    cmd = "find {} -type f -name \"*{}*\"".format(PDB_FOLDER, pdb_code)
    p = Popen(cmd, shell=True, stdout=PIPE, stdin=PIPE)
    out,err = p.communicate()
    out = out.decode("utf-8").strip().split()
    gzip_pdb = ""
    for item in out:
        if item.endswith(".gz"):
            gzip_pdb = item
            break

    cmd = "gunzip -c {} > /tmp/tmp_{}.pdb".format(gzip_pdb, label)
    p = Popen(cmd, shell=True, stdout=PIPE, stdin=PIPE)
    out,err = p.communicate()

    cmd = "grep -e \"R VALUE * (WORKING SET)\" /tmp/tmp_{}.pdb | head -n1".format(label)
    p = Popen(cmd, shell=True, stdout=PIPE, stdin=PIPE)
    out,err = p.communicate()
    out = out.decode("utf-8")
    try:
        r_value = float(out.strip().split()[-1])
    except:
        r_value = 0.1

    return r_value

def get_missing_residues(pdb_file):
    gap_dict = get_gaps(pdb_file)
    m_list = []
    for gaps in gap_dict.values():
        for v in gaps:
            if v.start == v.stop:
                m_list.append(v.start)
            else:
                for i in v:
                    m_list.append(i)
    return m_list

def get_representatives(interfaces_file,label):
    interfaces = pickle.load(open(interfaces_file,"rb"))

    keys2remove = []
    parser = PDBParser()
    representatives_cluster = {}
    for key,values in interfaces.items():
        representative = ""
        current_quality_score = -math.inf
        for pdb_file in values:
            pdb_identifier = ntpath.basename(pdb_file).replace(".pdb","").replace("interface_interface_","")
            pdb_code = pdb_identifier.split("_")[0]
            chain1 = pdb_identifier.split("_")[1]
            chain2 = pdb_identifier.split("_")[2]

            r_value = get_r_value(pdb_code, label)

            structure = parser.get_structure(pdb_identifier, "/tmp/tmp_{}.pdb".format(label))
            try:
                model = structure[0]
            except:
                break
            resolution = structure.header.get('resolution')
            if resolution == None:
                resolution = 1

            missing_residues = get_missing_residues(pdb_file.replace("interface_interface","interface"))
            m_value = float(len(missing_residues))/len(list(model.get_residues()))

            quality_score = ((1 - resolution) + (0.1 - r_value)) + (1 - m_value)
            if quality_score > current_quality_score:
                representative = pdb_file.replace("interface_interface","interface")
                current_quality_score = quality_score

        representatives_cluster[key] = representative
    
    representatives_file = os.path.join(os.getcwd(),"{}_representatives.pkl".format(label))
    pickle.dump(representatives_cluster, open(representatives_file,"wb"))
    return representatives_cluster

def get_enzyme_peptides(cluster):
    enzyme_interactions = {}
    protein_interactions = {}
    for cluster_id,pdb_file in cluster.items():
        if len(cluster[cluster_id]) == 0:
            import pdb;pdb.set_trace()
    for cluster_id,pdb_file in cluster.items():
        pdb_identifier = ntpath.basename(pdb_file).replace(".pdb","")
        try:
            pdb_code = pdb_identifier.split("_")[1]
        except:
            import pdb;pdb.set_trace()
        parser = PDBParser()
        structure = parser.get_structure(pdb_identifier, pdb_file)
        model = structure[0]
        chain1,chain2 = [item for item in model.get_chains()]
        residues_chain1 = [item for item in model[chain1.id].get_residues()]
        residues_chain2 = [item for item in model[chain2.id].get_residues()]

        chain2query = str()
        if len(residues_chain1) > 41:
            chain2query = chain1.id
        else:
            chain2query = chain2.id

        df_enzymes = pd.read_csv("pdb_chain_enzyme.csv")
        enzyme_entry = df_enzymes.loc[(df_enzymes['PDB'] == pdb_code) & (df_enzymes['CHAIN'] == chain2query)]
        if len(enzyme_entry) != 0:
            enzyme_interactions[cluster_id] = pdb_file
        else:
            protein_interactions[cluster_id] = pdb_file

    enzymes_file = os.path.join(os.getcwd(),"interfaces_clusters","enzyme_peptide_representatives.pkl".format(label))
    print("Enzyme-peptide interfaces:\t{}".format(len(enzyme_interactions)))
    pickle.dump(enzyme_interactions, open(enzymes_file,"wb"))
    proteins_file = os.path.join(os.getcwd(),"interfaces_clusters","protein_peptide_representatives.pkl".format(label))
    print("Protein-peptide interfaces:\t{}".format(len(protein_interactions)))
    pickle.dump(protein_interactions, open(proteins_file,"wb"))

    return True

def get_symmetrical_interfaces(cluster):
    symmetrical_interfaces = {}
    nonsymmetrical_interfaces = {}

    for cluster_id,pdb_file in cluster.items():
        side1,side2 = cluster_id.split("_")
        seq = SequenceMatcher(None, side1, side2)
        if seq.ratio() >= 0.75:
            symmetrical_interfaces[cluster_id] = pdb_file
        else:
            nonsymmetrical_interfaces[cluster_id] = pdb_file

    symmetrical_file = os.path.join(os.getcwd(),"interfaces_clusters","identical_symmetrical_representatives.pkl")
    print("Identical symmetric interfaces:\t{}".format(len(symmetrical_interfaces)))
    pickle.dump(symmetrical_interfaces, open(symmetrical_file,"wb"))
    nonsymmetrical_file = os.path.join(os.getcwd(),"interfaces_clusters","identical_nonsymmetrical_representatives.pkl")
    print("Identical non-symmetric interfaces:\t{}".format(len(nonsymmetrical_interfaces)))
    pickle.dump(nonsymmetrical_interfaces, open(nonsymmetrical_file,"wb"))

    return True


if __name__ == "__main__":
    pickle_pairs = sys.argv[1]
    label = sys.argv[2]

    representatives_cluster = get_representatives(pickle_pairs, label)
    if label == "protein_peptide":
        print("ENZYMES")
        get_enzyme_peptides(representatives_cluster)
    elif label == "identical_pairs":
        print("IDENTICAL PAIRS")
        get_symmetrical_interfaces(representatives_cluster)
