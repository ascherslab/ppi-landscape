from Bio.PDB import NeighborSearch, PDBIO, PDBParser, PDBList, Select, Selection, DSSP, ResidueDepth
from Bio.PDB.Polypeptide import three_to_one, is_aa
from collections import Counter
from subprocess import Popen, PIPE

import math
import matplotlib.pyplot as plt
import numpy as np
import ntpath
import os
import pandas as pd
import pickle
import sys
import warnings

warnings.filterwarnings("ignore")

VALID_PAIRS = ""

def get_interface(pdb_file):
    pdb_identifier = ntpath.basename(pdb_file).replace(".pdb","")
    pdb_code = pdb_identifier.split("_")[1]
    chain1 = pdb_identifier.split("_")[-2]
    chain2 = pdb_identifier.split("_")[-1]

    parser = PDBParser()
    structure = parser.get_structure(pdb_identifier, pdb_file)
    model = structure[0]

    interface1 = model[chain1]
    interface2 = model[chain2]

    residues_interface_chain2 = set()
    atoms_list = Selection.unfold_entities(interface2,'A')
    ns = NeighborSearch(atoms_list)
    neighbors = None
    for atom in interface1.get_atoms():
        center = atom.get_coord()
        neighbors = ns.search(center,5.0,level='R')
        if neighbors:
            residues_interface_chain2.update(neighbors)

    residues_interface_chain1 = set()
    atoms_list = Selection.unfold_entities(interface1,'A')
    ns = NeighborSearch(atoms_list)
    neighbors = None
    for atom in interface2.get_atoms():
        center = atom.get_coord()
        neighbors = ns.search(center,5.0,level='R')
        if neighbors:
            residues_interface_chain1.update(neighbors)

    residues_interface_chain1 = list(residues_interface_chain1)
    residues_interface_chain2 = list(residues_interface_chain2)
    residues_interface_chain1.sort()
    residues_interface_chain2.sort()
    seq1 = [item['CA'].get_coord() for item in residues_interface_chain1]
    seq2 = [item['CA'].get_coord() for item in residues_interface_chain2]

    return seq1,seq2

def main():
    interfaces_file = sys.argv[1]
    interface_clusters = pickle.load(open(interfaces_file,"rb"))

    features_dict = {}
    for cluster_id,pdb_file in interface_clusters.items():
        features_dict[cluster_id] = {}

        pdb_identifier = ntpath.basename(pdb_file).replace("interface_","").replace(".pdb","")
        pdb_code = pdb_identifier.split("_")[0]
        chain1 = pdb_identifier.split("_")[1]
        chain2 = pdb_identifier.split("_")[2]
        parsed_pdb_file = os.path.join(VALID_PAIRS,"interface_{}_{}_{}.pdb".format(pdb_code,chain1,chain2))

        try:
            residues_side1,residues_side2 = get_interface(parsed_pdb_file)
            xyz = np.array(residues_side1 + residues_side2)

            xs = np.array([item[0] for item in xyz])
            ys = np.array([item[1] for item in xyz])
            zs = np.array([item[2] for item in xyz])

            tmp_A = []
            tmp_b = []
            for i in range(len(xs)):
                tmp_A.append([xs[i], ys[i], 1])
                tmp_b.append(zs[i])
            b = np.matrix(tmp_b).T
            A = np.matrix(tmp_A)
            fit = (A.T * A).I * A.T * b
            errors = b - A * fit
            residual = np.linalg.norm(errors)

            transformed_z = fit[0]*xyz[:,0] + fit[1]*xyz[:,1] + fit[2]
            xyz_plane = np.array(np.c_[xyz[:,0], xyz[:,1], transformed_z.T])
            diff = np.array(xyz) - np.array(xyz_plane)
            N = len(xyz)
            rmsd = np.sqrt((diff * diff).sum() / N)
            features_dict[cluster_id] = rmsd
        except KeyError as e:
            features_dict[cluster_id] = 'error'
            pass

    file_identifier = ntpath.basename(interfaces_file).replace(".pkl","")
    pickle_file = "interfaces_clusters/{}_planarity.pkl".format(file_identifier)
    pickle.dump(features_dict, open(pickle_file,"wb"))
    print(pickle_file)

    return True


if __name__ == "__main__":
    main()
