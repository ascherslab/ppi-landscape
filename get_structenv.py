from Bio.PDB import NeighborSearch, PDBIO, PDBParser, PDBList, \
        Select, Selection, DSSP, ResidueDepth
from Bio.PDB.ResidueDepth import get_surface, residue_depth, ca_depth
from Bio.PDB.Polypeptide import three_to_one, is_aa
from collections import Counter
from subprocess import Popen, PIPE

import math
import ntpath
import os
import pandas as pd
import pickle
import sys
import warnings

warnings.filterwarnings("ignore")

VALID_PAIRS = ""

def get_interface(pdb_file, threshold=5.0):
    pdb_identifier = ntpath.basename(pdb_file).replace(".pdb","")
    pdb_code = pdb_identifier.split("_")[1]
    chain1 = pdb_identifier.split("_")[-2]
    chain2 = pdb_identifier.split("_")[-1]

    parser = PDBParser()
    structure = parser.get_structure(pdb_identifier, pdb_file)
    model = structure[0]

    interface1 = model[chain1]
    interface2 = model[chain2]

    residues_interface_chain2 = set()
    atoms_list = Selection.unfold_entities(interface2,'A')
    ns = NeighborSearch(atoms_list)
    neighbors = None
    for atom in interface1.get_atoms():
        center = atom.get_coord()
        neighbors = ns.search(center,threshold,level='R')
        if neighbors:
            residues_interface_chain2.update(neighbors)

    residues_interface_chain1 = set()
    atoms_list = Selection.unfold_entities(interface1,'A')
    ns = NeighborSearch(atoms_list)
    neighbors = None
    for atom in interface2.get_atoms():
        center = atom.get_coord()
        neighbors = ns.search(center,threshold,level='R')
        if neighbors:
            residues_interface_chain1.update(neighbors)

    residues_interface_chain1 = list(residues_interface_chain1)
    residues_interface_chain2 = list(residues_interface_chain2)
    residues_interface_chain1.sort()
    residues_interface_chain2.sort()
    seq1 = ["{}/{}/{}".format(item.parent.id, item.id[1], item.resname) \
            for item in residues_interface_chain1]
    seq2 = ["{}/{}/{}".format(item.parent.id, item.id[1], item.resname) \
            for item in residues_interface_chain2]

    return seq1,seq2

def main():
    interfaces_file = sys.argv[1]
    interface_clusters = pickle.load(open(interfaces_file,"rb"))

    parser = PDBParser()
    io_selection = PDBIO()
    features_dict = {}
    for cluster_id,pdb_file in interface_clusters.items():
        features_dict[cluster_id] = {}

        pdb_identifier = ntpath.basename(pdb_file).replace("interface_","").replace(".pdb","")
        pdb_code = pdb_identifier.split("_")[0]
        chain1 = pdb_identifier.split("_")[1]
        chain2 = pdb_identifier.split("_")[2]
        parsed_pdb_file = os.path.join(VALID_PAIRS,"interface_{}_{}_{}.pdb".format(pdb_code,chain1,chain2))

        structure = parser.get_structure(pdb_identifier, parsed_pdb_file)
        model = structure[0]
        try:
            dssp = DSSP(model, pdb_file)
        except Exception as e:
            print("DSSP failed")
            print(e)
            print(pdb_file)
            continue

        residues_side1,residues_side2 = get_interface(parsed_pdb_file)
        interface_residues = residues_side1 + residues_side2
        residues_side1_further,residues_side2_further = get_interface(parsed_pdb_file,threshold=6.0)
        interface_residues_further = residues_side1_further + residues_side2_further
        periphery_residues = [item for item in interface_residues_further if item not in interface_residues]

        for residue in periphery_residues:
            chain,position,resname = residue.split('/')
            key = '{}_{}'.format(chain,position)

            features_dict[cluster_id][key] = {}
            try:
                dssp_key = dssp[(chain,int(position))]
                features_dict[cluster_id][key] = {}
                features_dict[cluster_id][key]['sst'] = dssp_key[2]
                features_dict[cluster_id][key]['rsa'] = dssp_key[3]
                features_dict[cluster_id][key]['phi'] = dssp_key[4]
                features_dict[cluster_id][key]['psi'] = dssp_key[5]
            except Exception as e:
                print(e)
                print(pdb_file)
                features_dict[cluster_id][key]['sst'] = 'error'
                features_dict[cluster_id][key]['rsa'] = 'error'
                features_dict[cluster_id][key]['phi'] = 'error'
                features_dict[cluster_id][key]['psi'] = 'error'
                pass

    file_identifier = ntpath.basename(interfaces_file).replace(".pkl","")
    pickle_file = "interfaces_clusters/{}_structenv_periphery.pkl".format(file_identifier)
    pickle.dump(features_dict, open(pickle_file,"wb"))
    print(pickle_file)

    return True


if __name__ == "__main__":
    main()
