from Bio import SeqIO
from Bio.Alphabet import IUPAC
from Bio.Seq import Seq
from Bio.PDB import NeighborSearch, PDBIO, PDBParser, PDBList, Select, Selection, DSSP, ResidueDepth
from Bio.PDB.Polypeptide import three_to_one, is_aa 
from Bio.SeqRecord import SeqRecord
from difflib import SequenceMatcher
from subprocess import Popen, PIPE

import itertools
import ntpath
import numpy as np
import os
import pickle
import sys
import warnings

warnings.filterwarnings("ignore")

PDB_FOLDER = ''
BIOUNITS_FOLDER = ''
VALID_PAIRS = ''

def main():
    pdb_files = [item.replace(".pdb","") for item in os.listdir(VALID_PAIRS) \
            if item.endswith(".pdb")]

    cluster_unique = {}

    cdhit_clusters = os.path.join(os.getcwd(),"cdhit","cdhit_clusters.pkl")
    cluster_dict = pickle.load(open(cdhit_clusters,"rb"))

    for pdb_identifier in pdb_files:
        pdb_code = pdb_identifier.split("_")[1]
        chain1 = pdb_identifier.split("_")[2]
        chain2 = pdb_identifier.split("_")[3]

        pdb_file = os.path.join(VALID_PAIRS,"interface_" + pdb_identifier + ".pdb")

        cluster_key_chain1 = "{}_{}_{}-{}".format(pdb_code,chain1,chain2,chain1)
        cluster_key_chain2 = "{}_{}_{}-{}".format(pdb_code,chain1,chain2,chain2)

        unique_key = ""
        if cluster_key_chain1 not in cluster_dict:
            unique_key = "{}-PEPTIDE".format(cluster_dict[cluster_key_chain2])
        elif cluster_key_chain2 not in cluster_dict:
            unique_key = "{}-PEPTIDE".format(cluster_dict[cluster_key_chain1])
        else:
            if int(cluster_dict[cluster_key_chain1]) < int(cluster_dict[cluster_key_chain2]):
                unique_key = "{}-{}".format(cluster_dict[cluster_key_chain1],cluster_dict[cluster_key_chain2])
            else:
                unique_key = "{}-{}".format(cluster_dict[cluster_key_chain2],cluster_dict[cluster_key_chain1])

        if unique_key in cluster_unique:
            cluster_unique[unique_key].append(pdb_file)
        else:
            cluster_unique[unique_key] = [pdb_file]

    cluster_unique_file = "unique_pairs.pkl"
    print("Unique Pairs:\t{}".format(len(cluster_unique)))
    pickle.dump(cluster_unique,open(cluster_unique_file,"wb"))

    return True

if __name__ == "__main__":
    main()
