import pickle

def main():

    UNIQUES_PAIRS = pickle.load(open("unique_pairs.pkl","rb"))
    NON_PEPTIDE_INTERACTIONS = [item for item in UNIQUES_PAIRS.keys() if '-PEPTIDE' not in item]
    identical = []
    non_identical = []
    for i in NON_PEPTIDE_INTERACTIONS:
        c1,c2 = i.split("-")
        if c1 == c2:
            identical.append(i)
        else:
            non_identical.append(i)
    peptide_interactions = [item for item in UNIQUES_PAIRS.keys() if '-PEPTIDE' in item]
    print(len(identical))
    print(len(non_identical))
    print(len(peptide_interactions))

    pickle.dump(identical,open("unique_identical_pairs.pkl","wb"))
    pickle.dump(non_identical,open("unique_non_identical_pairs.pkl","wb"))
    pickle.dump(peptide_interactions,open("unique_protein_peptide_pairs.pkl","wb"))

    return True


if __name__ == "__main__":
    main()
