from Bio.PDB import NeighborSearch, PDBIO, PDBParser, PDBList, \
        Select, Selection, DSSP, ResidueDepth
from Bio.PDB.ResidueDepth import get_surface, residue_depth, ca_depth
from Bio.PDB.Polypeptide import three_to_one, is_aa
from collections import Counter
from subprocess import Popen, PIPE

import math
import ntpath
import os
import pandas as pd
import pickle
import sys
import warnings

warnings.filterwarnings("ignore")

VALID_PAIRS = ""
ARPEGGIO_CODE_FOLDER = ""

def getAtomRing(input_file, rings_file):
    dict_contacts = {
        'ATOM_RING_CARBONPI': 0, 
        'ATOM_RING_CATIONPI': 0, 
        'ATOM_RING_DONORPI': 0, 
        'ATOM_RING_HALOGENPI': 0, 
        'ATOM_RING_METSULPHURPI': 0,
        'RING_ATOM_CARBONPI': 0, 
        'RING_ATOM_CATIONPI': 0, 
        'RING_ATOM_DONORPI': 0, 
        'RING_ATOM_HALOGENPI': 0, 
        'RING_ATOM_METSULPHURPI': 0
    }
    with open(input_file, 'r') as input:
        for each in input.readlines():
            striped = each.strip().split('\t')
            target_residue = striped[2]
            target_atom = striped[3]
            interaction_type = striped[4][2:-2]
            cmd = "grep \"{}\" {} | grep \"{}\"".format(target_residue, rings_file, target_atom)
            p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
            out,err = p.communicate()
            if len(out.decode("utf-8")) != 0:
                try:
                    dict_contacts["ATOM_RING_"+interaction_type] += 1
                except KeyError as e:
                    for item in interaction_type.replace("'","").split(","):
                        dict_contacts["ATOM_RING_" + item.strip()] += 1
            else:
                try:
                    dict_contacts["RING_ATOM_"+interaction_type] += 1
                except KeyError as e:
                    for item in interaction_type.replace("'","").split(","):
                        dict_contacts["RING_ATOM_" + item.strip()] += 1

    return dict_contacts

def getRingRing(input_file):
    dict_contacts = {'RING_RING':0}
    with open(input_file, 'r') as input:
        for each in input.readlines():
            dict_contacts['RING_RING'] += 1
    return dict_contacts

def getAmideAmide(input_file):
    dict_contacts = {'AMIDE_AMIDE':0}
    with open(input_file, 'r') as input:
        for each in input.readlines():
            dict_contacts['AMIDE_AMIDE'] += 1
    return dict_contacts

def getAmideRing(input_file, rings_file):
    dict_contacts = {'ATOM_RING_AMIDERING':0, 'RING_ATOM_AMIDERING':0}
    with open(input_file, 'r') as input:
        for each in input.readlines():
            striped = each.strip().split('\t')
            target_residue = striped[4]
            target_atom = striped[5]
            interaction_type = striped[6]
            cmd = "grep \"{}\" {} | grep \"{}\"".format(target_residue, rings_file, target_atom)
            p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
            out,err = p.communicate()
            if len(out.decode("utf-8")) != 0:
                dict_contacts["ATOM_RING_"+interaction_type] += 1
            else:
                dict_contacts["RING_ATOM_"+interaction_type] += 1

    return dict_contacts

def getSumContacts(input_file, condition="ALL"):
    result = {}

    Clash = []
    Covalent = []
    VDWClash = []
    VDW = []
    Proximal = []
    Metal = []
    Hydrophobic = []

    Hbond = []
    WeakHbond = []
    Halogen = []
    Ionic = []
    Polar = []
    WeakPolar = []

    Aromatic = []
    Carbonyl = []

    if condition == 'INTER':
        with open(input_file, 'r') as input:
            for each in input.readlines():
                striped = each.strip().split('\t')

                if striped[17] == 'INTER':
                    Clash.append(int(striped[2]))
                    Covalent.append(int(striped[3]))
                    VDWClash.append(int(striped[4]))
                    VDW.append(int(striped[5]))
                    Proximal.append(int(striped[6]))
                    Metal.append(int(striped[11]))
                    Hydrophobic.append(int(striped[13]))

                    Hbond.append(int(striped[7]))
                    WeakHbond.append(int(striped[8]))
                    Halogen.append(int(striped[9]))
                    Ionic.append(int(striped[10]))
                    Polar.append(int(striped[15]))
                    WeakPolar.append(int(striped[16]))

                    Aromatic.append(int(striped[12]))
                    Carbonyl.append(int(striped[14]))

                filename = input_file.split('/')[-1]

                result['Clash'] = sum(Clash)
                result['Covalent'] = sum(Covalent)
                result['VDWClash'] = sum(VDWClash)
                result['VDW'] = sum(VDW)
                result['Proximal'] = sum(Proximal)
                result['Hbond'] = sum(Hbond)
                result['WeakHbond'] = sum(WeakHbond)
                result['Halogen'] = sum(Halogen)
                result['Ionic'] = sum(Ionic)
                result['Metal'] = sum(Metal)
                result['Aromatic'] = sum(Aromatic)
                result['Hydrophobic'] = sum(Hydrophobic)
                result['Carbonyl'] = sum(Carbonyl)
                result['Polar'] = sum(Polar)
                result['WeakPolar'] = sum(WeakPolar)

    elif condition == 'ALL':

        with open(input_file, 'r') as input:
            for each in input.readlines():
                striped = each.strip().split('\t')
                Clash.append(int(striped[2]))
                Covalent.append(int(striped[3]))
                VDWClash.append(int(striped[4]))
                VDW.append(int(striped[5]))
                Proximal.append(int(striped[6]))
                Metal.append(int(striped[11]))
                Hydrophobic.append(int(striped[13]))

                Hbond.append(int(striped[7]))
                WeakHbond.append(int(striped[8]))
                Halogen.append(int(striped[9]))
                Ionic.append(int(striped[10]))
                Polar.append(int(striped[15]))
                WeakPolar.append(int(striped[16]))

                Aromatic.append(int(striped[12]))
                Carbonyl.append(int(striped[14]))

            filename = input_file.split('/')[-1]

            result['Clash'] = sum(Clash)
            result['Covalent'] = sum(Covalent)
            result['VDWClash'] = sum(VDWClash)
            result['VDW'] = sum(VDW)
            result['Proximal'] = sum(Proximal)
            result['Hbond'] = sum(Hbond)
            result['WeakHbond'] = sum(WeakHbond)
            result['Halogen'] = sum(Halogen)
            result['Ionic'] = sum(Ionic)
            result['Metal'] = sum(Metal)
            result['Aromatic'] = sum(Aromatic)
            result['Hydrophobic'] = sum(Hydrophobic)
            result['Carbonyl'] = sum(Carbonyl)
            result['Polar'] = sum(Polar)
            result['WeakPolar'] = sum(WeakPolar)

    return result

def get_interface(pdb_file):
    pdb_identifier = ntpath.basename(pdb_file).replace(".pdb","")
    pdb_code = pdb_identifier.split("_")[1]
    chain1 = pdb_identifier.split("_")[-2]
    chain2 = pdb_identifier.split("_")[-1]

    parser = PDBParser()
    structure = parser.get_structure(pdb_identifier, pdb_file)
    model = structure[0]
    chains = [item.id for item in model.get_chains()]
    chain1 = chains[0]
    chain2 = chains[1]

    interface1 = model[chain1]
    interface2 = model[chain2]

    residues_interface_chain2 = set()
    atoms_list = Selection.unfold_entities(interface2,'A')
    ns = NeighborSearch(atoms_list)
    neighbors = None
    for atom in interface1.get_atoms():
        center = atom.get_coord()
        neighbors = ns.search(center,5.0,level='R')
        if neighbors:
            residues_interface_chain2.update(neighbors)

    residues_interface_chain1 = set()
    atoms_list = Selection.unfold_entities(interface1,'A')
    ns = NeighborSearch(atoms_list)
    neighbors = None
    for atom in interface2.get_atoms():
        center = atom.get_coord()
        neighbors = ns.search(center,5.0,level='R')
        if neighbors:
            residues_interface_chain1.update(neighbors)

    residues_interface_chain1 = list(residues_interface_chain1)
    residues_interface_chain2 = list(residues_interface_chain2)
    residues_interface_chain1.sort()
    residues_interface_chain2.sort()
    seq1 = ["/{}/{}/".format(item.parent.id, item.id[1], three_to_one(item.resname)) \
            for item in residues_interface_chain1]
    seq2 = ["/{}/{}/".format(item.parent.id, item.id[1]) \
            for item in residues_interface_chain2]

    return seq1,seq2

def renumber_pdb(pdb_file):
    folder = os.path.dirname(pdb_file)
    pdb_identifier = ntpath.basename(pdb_file).replace(".pdb","")
    renumbered_pdb_file = os.path.join(folder,"{}_renumbered.pdb".format(pdb_identifier))

    parser = PDBParser()
    structure = parser.get_structure(pdb_file, pdb_file)
    model = structure[0]

    chains = [item.id for item in model.get_chains()]
    residues_keys = list()
    for c in chains:
        residues_keys += ['{}_{}{}'.format(c, str(item.id[1]), item.id[2]).strip() for item in structure[0][c].get_residues()]
    residues_values = range(0,len(residues_keys))
    residues_mapping = dict(zip(residues_keys,residues_values))

    with open(pdb_file,'r') as pdb_reader:
        lines = pdb_reader.readlines()
        pdb_to_write = open(renumbered_pdb_file,'w')
        for line in lines:
            list_line = line.split()
            if line.startswith("END"):
                pdb_to_write.write(line)
            elif  line.startswith("TER"):
                pdb_to_write.write("TER\n")
            elif line.startswith("ATOM"):
                c = line[21].strip()
                res_n = line[22:27].strip()
                res_mapping = residues_mapping["{}_{}".format(c,res_n)]
                formatted_line = "%s%4i %s" % (line[0:22],res_mapping,line[27:])
                pdb_to_write.write(formatted_line)
            else:
                pass
        pdb_to_write.close()

    return renumbered_pdb_file, residues_mapping

def main():
    interfaces_file = sys.argv[1]
    interface_clusters = pickle.load(open(interfaces_file,"rb"))

    parser = PDBParser()
    io_selection = PDBIO()
    features_dict = {}
    for cluster_id,pdb_file in interface_clusters.items():
        features_dict[cluster_id] = {}

        pdb_identifier = ntpath.basename(pdb_file).replace("interface_","").replace(".pdb","")
        pdb_code = pdb_identifier.split("_")[0]
        chain1 = pdb_identifier.split("_")[1]
        chain2 = pdb_identifier.split("_")[2]
        parsed_pdb_file = os.path.join(VALID_PAIRS,"interface_{}_{}_{}.pdb".format(pdb_code,chain1,chain2))
        renumbered_pdb_file, residues_mapping = renumber_pdb(parsed_pdb_file)

        residues_side1,residues_side2 = get_interface(renumbered_pdb_file)
        interface_residues = residues_side1 + residues_side2

        selection_file = parsed_pdb_file.replace(".pdb",".selection")
        with open(selection_file,"w") as writer:
            for residue in interface_residues:
                writer.write(residue + "\n")

        cmd = "python {}/arpeggio.py {} -sf {} -wh -ph 7.4".format(ARPEGGIO_CODE_FOLDER,\
                renumbered_pdb_file, selection_file)
        print(cmd)
        p = Popen(cmd, stdin=PIPE, stdout=PIPE, shell=True)
        out,err = p.communicate()

        amam_file = renumbered_pdb_file.replace(".pdb",".amam")
        amri_file = renumbered_pdb_file.replace(".pdb",".amri")
        ari_file = renumbered_pdb_file.replace(".pdb",".ari")
        ri_file = renumbered_pdb_file.replace(".pdb",".ri")
        rings_file = renumbered_pdb_file.replace(".pdb",".rings")
        contacts_file = renumbered_pdb_file.replace(".pdb",".contacts")

        contacts_counter = {'Clash':'error', 'Covalent':'error', 'VDWClash':'error', \
                'VDW':'error', 'Proximal':'error', 'Hbond':'error', 'WeakHbond':'error',\
                'Halogen':'error', 'Ionic':'error', 'Metal':'error', 'Aromatic':'error',\
                'Hydrophobic':'error', 'Carbonyl':'error', 'Polar':'error', 'WeakPolar':'error'}
        atom_ring_counter = {'ATOM_RING_CARBONPI':'error', 'ATOM_RING_CATIONPI':'error', 'ATOM_RING_DONORPI':'error', \
                'ATOM_RING_HALOGENPI':'error', 'ATOM_RING_METSULPHURPI':'error', 'RING_ATOM_CARBONPI':'error', \
                'RING_ATOM_CATIONPI':'error', 'RING_ATOM_DONORPI':'error', 'RING_ATOM_HALOGENPI':'error', \
                'RING_ATOM_METSULPHURPI':'error'}
        ring_ring_counter = {'RING_RING':'error'}
        amide_amide_counter = {'AMIDE_AMIDE':'error'}
        amide_ring_counter = {'ATOM_RING_AMIDERING':'error', 'RING_ATOM_AMIDERING':'error'}
        if os.path.exists(contacts_file):
            contacts_counter = getSumContacts(contacts_file)
            atom_ring_counter = getAtomRing(ari_file, rings_file)
            ring_ring_counter = getRingRing(ri_file)
            amide_amide_counter = getAmideAmide(amam_file)
            amide_ring_counter = getAmideRing(amri_file, rings_file)

        features_dict[cluster_id].update(contacts_counter)
        features_dict[cluster_id].update(atom_ring_counter)
        features_dict[cluster_id].update(ring_ring_counter)
        features_dict[cluster_id].update(amide_amide_counter)
        features_dict[cluster_id].update(amide_ring_counter)


    file_identifier = ntpath.basename(interfaces_file).replace(".pkl","")
    pickle_file = "interfaces_clusters/{}_contacts.pkl".format(file_identifier)
    pickle.dump(features_dict, open(pickle_file,"wb"))
    print(pickle_file)

    return True


if __name__ == "__main__":
    main()
