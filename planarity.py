from Bio.PDB import NeighborSearch, PDBIO, PDBParser, PDBList, Select, Selection, DSSP, ResidueDepth
from Bio.PDB.Polypeptide import three_to_one, is_aa
from collections import Counter
from subprocess import Popen, PIPE

import math
import matplotlib.pyplot as plt
import numpy as np
import ntpath
import os
import pandas as pd
import pickle
import sys
import warnings

warnings.filterwarnings("ignore")

VALID_PAIRS = "/storage/ppi_landscapes/valid_pairs/"
PICKLES_DIR = os.path.join("/home/local/BHRI/crodrigues/Documents/ppi_landscapes","interfaces_clusters")

def get_interface(pdb_file):
    pdb_identifier = ntpath.basename(pdb_file).replace(".pdb","")
    pdb_code = pdb_identifier.split("_")[1]
    chain1 = pdb_identifier.split("_")[-2]
    chain2 = pdb_identifier.split("_")[-1]

    parser = PDBParser()
    structure = parser.get_structure(pdb_identifier, pdb_file)
    model = structure[0]

    interface1 = model[chain1]
    interface2 = model[chain2]

    residues_interface_chain2 = set()
    atoms_list = Selection.unfold_entities(interface2,'A')
    ns = NeighborSearch(atoms_list)
    neighbors = None
    for atom in interface1.get_atoms():
        center = atom.get_coord()
        neighbors = ns.search(center,5.0,level='R')
        if neighbors:
            residues_interface_chain2.update(neighbors)

    residues_interface_chain1 = set()
    atoms_list = Selection.unfold_entities(interface1,'A')
    ns = NeighborSearch(atoms_list)
    neighbors = None
    for atom in interface2.get_atoms():
        center = atom.get_coord()
        neighbors = ns.search(center,5.0,level='R')
        if neighbors:
            residues_interface_chain1.update(neighbors)

    residues_interface_chain1 = list(residues_interface_chain1)
    residues_interface_chain2 = list(residues_interface_chain2)
    residues_interface_chain1.sort()
    residues_interface_chain2.sort()
    seq1 = [item['CA'].get_coord() for item in residues_interface_chain1]
    seq2 = [item['CA'].get_coord() for item in residues_interface_chain2]

    return seq1,seq2

def planeFit(points):
    """
    p, n = planeFit(points)

    Given an array, points, of shape (d,...)
    representing points in d-dimensional space,
    fit an d-dimensional plane to the points.
    Return a point, p, on the plane (the point-cloud centroid),
    and the normal, n.
    """
    points = np.reshape(points, (np.shape(points)[0], -1)) # Collapse trialing dimensions
    assert points.shape[0] <= points.shape[1], "There are only {} points in {} dimensions.".format(points.shape[1], points.shape[0])
    ctr = points.mean(axis=1)
    x = points - ctr[:,np.newaxis]
    M = np.dot(x, x.T) # Could also use np.cov(x) here.
    return ctr, np.linalg.svd(M)[0][:,-1]

REPRESENTATIVES_CLUSTERS = {
    'enzyme_peptide': pickle.load(open(os.path.join(PICKLES_DIR,"enzyme_peptide_representatives.pkl"),'rb')),
    'protein_peptide': pickle.load(open(os.path.join(PICKLES_DIR,"protein_peptide_representatives.pkl"),'rb')),
    'nonidentical': pickle.load(open(os.path.join(PICKLES_DIR,"nonidentical_pairs_representatives.pkl"),'rb')),
    'identical_symmetrical': pickle.load(open(os.path.join(PICKLES_DIR,"identical_symmetrical_representatives.pkl"),'rb')),
    'identical_nonsymmetrical': pickle.load(open(os.path.join(PICKLES_DIR,"identical_nonsymmetrical_representatives.pkl"),'rb')),
}

def main():
    csv_file = "outliers_planarity.csv"
    df = pd.read_csv(csv_file)
    for idx,row in df.iterrows():
        pdb_file = REPRESENTATIVES_CLUSTERS[row['interface_type']][row['cluster_id']]
        pdb_identifier = ntpath.basename(pdb_file).replace("interface_","").replace(".pdb","")
        pdb_code = pdb_identifier.split("_")[0]
        chain1 = pdb_identifier.split("_")[1]
        chain2 = pdb_identifier.split("_")[2]
        parsed_pdb_file = os.path.join(VALID_PAIRS,"interface_{}_{}_{}.pdb".format(pdb_code,chain1,chain2))

        residues_side1,residues_side2 = get_interface(parsed_pdb_file)

        xyz = np.array(residues_side1 + residues_side2)

        xs = np.array([item[0] for item in xyz])
        ys = np.array([item[1] for item in xyz])
        zs = np.array([item[2] for item in xyz])

        tmp_A = []
        tmp_b = []
        for i in range(len(xs)):
            tmp_A.append([xs[i], ys[i], 1])
            tmp_b.append(zs[i])
        b = np.matrix(tmp_b).T
        A = np.matrix(tmp_A)
        fit = (A.T * A).I * A.T * b
        errors = b - A * fit
        residual = np.linalg.norm(errors)

        transformed_z = fit[0]*xyz[:,0] + fit[1]*xyz[:,1] + fit[2]
        xyz_plane = np.array(np.c_[xyz[:,0], xyz[:,1], transformed_z.T])
        print("RMSD")
        diff = np.array(xyz) - np.array(xyz_plane)
        N = len(xyz)
        rmsd = np.sqrt((diff * diff).sum() / N)
        print(rmsd)

        #x = np.arange(np.min(xyz[:, 0]), np.max(xyz[:, 0]), 0.1)
        #y = np.arange(np.min(xyz[:, 1]), np.max(xyz[:, 1]), 0.1)
#
        #xx, yy = np.meshgrid(x, y)
#
        #import pdb;pdb.set_trace()
        #zz = fit[0]*xx + fit[1]*yy + fit[2]

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        ax.scatter(xyz[:, 0], xyz[:, 1], xyz[:, 2], c='r')
        ax.scatter(xyz[:, 0], xyz[:, 1], transformed_z, c='blue')
        #ax.plot_wireframe(xx, yy, zz)

        plt.savefig('aux.png')
        import pdb;pdb.set_trace()

if __name__ == "__main__":
    main()
