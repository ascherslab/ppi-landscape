from Bio.PDB import NeighborSearch, PDBIO, PDBParser, PDBList, \
        Select, Selection, DSSP, ResidueDepth
from Bio.PDB.ResidueDepth import get_surface, residue_depth, ca_depth
from Bio.PDB.Polypeptide import three_to_one, is_aa
from collections import Counter
from subprocess import Popen, PIPE

import io
import math
import ntpath
import numpy as np
import os
import pandas as pd
import pickle
import sys
import warnings

warnings.filterwarnings("ignore")

VALID_PAIRS = "/storage/ppi_landscapes/valid_pairs/"

def get_interface(pdb_file):
    pdb_identifier = ntpath.basename(pdb_file).replace(".pdb","")
    pdb_code = pdb_identifier.split("_")[1]
    chain1 = pdb_identifier.split("_")[-2]
    chain2 = pdb_identifier.split("_")[-1]

    parser = PDBParser()
    structure = parser.get_structure(pdb_identifier, pdb_file)
    model = structure[0]

    interface1 = model[chain1]
    interface2 = model[chain2]

    residues_interface_chain2 = set()
    atoms_list = Selection.unfold_entities(interface2,'A')
    ns = NeighborSearch(atoms_list)
    neighbors = None
    for atom in interface1.get_atoms():
        center = atom.get_coord()
        neighbors = ns.search(center,5.0,level='R')
        if neighbors:
            residues_interface_chain2.update(neighbors)

    residues_interface_chain1 = set()
    atoms_list = Selection.unfold_entities(interface1,'A')
    ns = NeighborSearch(atoms_list)
    neighbors = None
    for atom in interface2.get_atoms():
        center = atom.get_coord()
        neighbors = ns.search(center,5.0,level='R')
        if neighbors:
            residues_interface_chain1.update(neighbors)

    residues_interface_chain1 = list(residues_interface_chain1)
    residues_interface_chain2 = list(residues_interface_chain2)
    residues_interface_chain1.sort()
    residues_interface_chain2.sort()
    seq1 = ["{}/{}/{}".format(item.parent.id, item.id[1], item.resname) \
            for item in residues_interface_chain1]
    seq2 = ["{}/{}/{}".format(item.parent.id, item.id[1], item.resname) \
            for item in residues_interface_chain2]

    return {chain1: seq1, chain2: seq2}

def main():
    interfaces_file = sys.argv[1]
    #label = sys.argv[2]
    interface_clusters = pickle.load(open(interfaces_file,"rb"))
    file_identifier = ntpath.basename(interfaces_file).replace(".pkl","")
    label = file_identifier.replace("_representatives","").replace("_pairs","")

    columns = ['cluster_id','interface_type','pdb']
    df  = pd.read_csv(io.StringIO(""), names=columns, dtype=dict(zip(columns,[str,str,str])))

    parser = PDBParser()
    output_list = []
    for cluster_id,pdb_file in interface_clusters.items():
        pdb_identifier = ntpath.basename(pdb_file).replace("interface_","").replace(".pdb","")
        pdb_code = pdb_identifier.split("_")[0]
        chain1 = pdb_identifier.split("_")[1]
        chain2 = pdb_identifier.split("_")[2]
        parsed_pdb_file = os.path.join(VALID_PAIRS,"interface_{}_{}_{}.pdb".format(pdb_code,chain1,chain2))

        output_list.append({'cluster_id':cluster_id,'interface_type':label,'pdb':pdb_identifier})

    df  = df.append(output_list, ignore_index=True)
    df.to_csv(os.path.join("/home/local/BHRI/crodrigues/Documents/ppi_landscapes","plots",\
            "{}_cluster_pdb.csv".format(label)), index=False)
    print(os.path.join("/home/local/BHRI/crodrigues/Documents/ppi_landscapes","plots",\
            "{}_cluster_pdb".format(label)))

    return True


if __name__ == "__main__":
    main()
