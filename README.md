# PPI Landscape

Here we describe the analysis and properties calculated for our work on PPI landscapes. Original data has been summarised in the form of **.pkl** files under the *interfaces_clusters* folder.

## Install

### Dependecies
All scripts were developed using Python version 3.7 and package dependencies can be installed via anaconda using the `requirements.yml` file available in this repository.
Third party applications are summarised below:

- [CD-HIT](https://pubmed.ncbi.nlm.nih.gov/16731699/)
- [NACCESS](http://www.bioinf.manchester.ac.uk/naccess/)
- [NIP and NSc](http://pallab.serc.iisc.ernet.in/nip_nsc.html)
- [DSSP](https://swift.cmbi.umcn.nl/gv/dssp/)
- [Arpeggio](https://github.com/harryjubb/arpeggio)
- [Ghecom](https://pdbj.org/ghecom/)
- [pdb-tools](https://github.com/haddocking/pdb-tools)

1. Install anaconda environment
```
conda env create -f requirements.yml
```
2. Download PDB database. Here we used the PDB database downloaded from 14 of April 2021.

## Parsing PDB structures

For each PDB file run the `seed.py` script to retrieve valid pairwise structures.

The following variables need to be set prior to running the script:

- **ALL_PDBS_FOLDER**: *all* folder from the downloaded PDB. These represent the standard structures shown on the PDB website.
- **BIOUNITS_FOLDER**: *biounit* folder from the PDB database, comprising all biological unit structures available in the database.
- **PPI_INTERFACES**: Local folder to store temporary PPI interfaces which are generated during the script execution
- **VALID_INTERFACES**: Local folder to store all valid PPI interfaces identified during the script execution
- **PDB_GAP_BIN**: Binary for finding gap regions from the [pdb-tools](https://github.com/haddocking/pdb-tools) package

```
python seed.py PDB_FILE
```

## Clustering chains using CD-HIT

Use the .fasta files generated on the previous step and located under the location defined on **VALID_INTERFACES** to build an input file that will be used by CD-HIT to cluster nearly-identical protein sequences with the following command:

```
find VALID_INTERFACES -type f -exec cat {} + > input.fasta
```

Run CD-Hit to cluster nearly identical proteins using a 95% sequence identity threshold:
```
cd-hit input.fasta -o output.out -c 0.95 -T 0
```

Parse output files using `parse_cdhit.py` script:

```
python parse_cdhit.py OUTPUT_CDHIT
```

The script above will output a pickle file named *cdhit_clusters.pkl*.

## Grouping interfaces by type

First, compile all unique pairs based on CD-HIT output clusters:

The following variables need to be set prior to running the script:

- **PDB_FOLDER**: *all* folder from the downloaded PDB. These represent the standard structures shown on the PDB website.
- **BIOUNITS_FOLDER**: *biounit* folder from the PDB database, comprising all biological unit structures available in the database.
- **VALID_INTERFACES**: Local folder to store all valid PPI interfaces identified during the script execution

```
python get_unique_pairs.py
```

The above script will generate an output pickle file named *unique_pairs.pkl*.

Second, compile all PPIs involving peptides and two globular proteins, clustering the latter into Identical and Non-identical pairs:

```
python split_pairs.py
```

The above script will generate 3 pickle files named *unique_pairs.pkl*.

- **unique_identical_pairs.pkl**
- **unique_non_identical_pairs.pkl**
- **unique_protein_peptide_pairs.pkl**

Finally, use the `group_interfaces.py` script to group each unique pair into each of the 5 types of interfaces defined in this study (Isologous homopair, Non-isologous homopair, Heteropair, Protein-peptide and Enzyme-peptide).

The following variables need to be set prior to running the script:

- **PDB_FOLDER**: *all* folder from the downloaded PDB. These represent the standard structures shown on the PDB website.
- **PDB_GAP_BIN**: Binary for finding gap regions from the [pdb-tools](https://github.com/haddocking/pdb-tools) package

```
python group_interfaces.py unique_identical_pairs.pkl

python group_interfaces.py unique_non_identical_pairs.pkl

python group_interfaces.py unique_protein_peptide_pairs.pkl
```

The above script will generate pickle files for each type of interaction with the representative pairwise interaction for each unique PPI under the folder *interfaces_clusters*.

## Calculating interface properties

Scripts are divided per type of property and use the pickle files for each PPI type generated on previous step.

### Interface segmentation

Calculate interface segmentation using a threshold gap of 4 residues.

The following variables need to be set prior to running the script:

- **VALID_INTERFACES**: Local folder to store all valid PPI interfaces identified during the script execution
```
python get_interface_continuity.py PPI_TYPE_CLUSTER_PICKLE
```

### Solvent Accessible Surface Area

Calculate solvent accessibility using NACCESS:

The following variables need to be set prior to running the script:

- **VALID_INTERFACES**: Local folder to store all valid PPI interfaces identified during the script execution
- **NACCESS_BIN**: Path to the binary for the naccess program

```
python get_sasa.py PPI_TYPE_CLUSTER_PICKLE
```

### Structure environment for interface residues
Calculate common structure environment properties for each interface residue using DSSP:

The following variables need to be set prior to running the script:

- **VALID_INTERFACES**: Local folder to store all valid PPI interfaces identified during the script execution

```
python get_structenv.py PPI_TYPE_CLUSTER_PICKLE
```

### Interface planarity

Calculate interface planarity using RMSD of interface residues Cɑ atoms from a least-squares fitted plane through the interface:

The following variables need to be set prior to running the script:

- **VALID_INTERFACES**: Local folder to store all valid PPI interfaces identified during the script execution

```
python get_planarity.py PPI_TYPE_CLUSTER_PICKLE
```

### Interatomic interactions

Calculate interatomic interactions for interface residues using Arpeggio:

The following variables need to be set prior to running the script:

- **VALID_INTERFACES**: Local folder to store all valid PPI interfaces identified during the script execution
- **ARPEGGIO_CODE_FOLDER**: Path to arpeggio source code

```
python get_contacts.py PPI_TYPE_CLUSTER_PICKLE
```

### Concavities (Pockets)

Calculate concavities at PPI interfaces using Ghecom:

The following variables need to be set prior to running the script:

- **VALID_INTERFACES**: Local folder to store all valid PPI interfaces identified during the script execution

```
python get_pockets.py PPI_TYPE_CLUSTER_PICKLE
```

### Interface Packing and Shape Complementarity

Calculate interface packing and shape complementarity via NIP and NSc:

The following variables need to be set prior to running the script:

- **VALID_INTERFACES**: Local folder to store all valid PPI interfaces identified during the script execution
- **NACCESS_PATH**: Path to the folder where the binary for the naccess program is located

```
python get_nip_nsc.py PPI_TYPE_CLUSTER_PICKLE
```
